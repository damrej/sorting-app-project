package com.damian;

import java.util.Arrays;

public class SortingEngine {
    private static final int MAX_ELEMENTS = 10;

    public static String sorting(String[] numbers) throws IllegalArgumentException{

        int[] intNumbers = new int[Math.min(MAX_ELEMENTS, numbers.length)];

        try {
            for (int i = 0; i < intNumbers.length; i++) {
                intNumbers[i] = Integer.parseInt(numbers[i]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("command line arguments should be integers");
        }

        Arrays.sort(intNumbers);

        return changeArrayToString(intNumbers);
    }

    private static String changeArrayToString(int[] numbers) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < numbers.length; i++) {
            sb.append(numbers[i]);
            if (i < numbers.length - 1) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }
}
