package com.damian;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingEngineParameterizedTest {
    
    String[] input;
    String expected;

    public SortingEngineParameterizedTest(String[] input, String expected) {
        this.input = input;
        this.expected = expected;
    }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {new String[] {"2","-3","0"},"-3 0 2"},
                    {new String[] {"2","-4","0","13","-10"},"-10 -4 0 2 13"},
                    {new String[] {"2","-4","0","13","-10","122","121"},"-10 -4 0 2 13 121 122"},
                    {new String[] {"2","-4","0","13","-10","1"},"-10 -4 0 1 2 13"},
            });
        }

        @Test
        public void testSortingAppSimpleCases() {
            assertEquals(expected, SortingEngine.sorting(input));
        }

}