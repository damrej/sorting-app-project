package com.damian;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SortingEngineTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNonIntegersCase(){

        String[] input = {"3","5","whatever","74","13"};
        SortingEngine.sorting(input);
    }

    @Test
    public void testEmptyCase(){

        String[] input = {};
        String expected = "";
        Assert.assertEquals(expected,SortingEngine.sorting(input));
    }

    @Test
    public void testSingleElementCase() {

        String[] input = {"3"};
        String expected = "3";
        Assert.assertEquals(expected,SortingEngine.sorting(input));
    }

    @Test
    public void testMaxElementsCase() {

        String[] input = {"3","-5","5","-1","74","133","4","0","77","13"};
        String expected = "-5 -1 0 3 4 5 13 74 77 133";
        Assert.assertEquals(expected,SortingEngine.sorting(input));
    }

    @Test
    public void testOverMaxElementsCases() {

        String[] input = {"3","1233","-5","5","-1","74","133","4","0","77","13"};
        String expected = "-5 -1 0 3 4 5 74 77 133 1233";
        Assert.assertEquals(expected,SortingEngine.sorting(input));
    }

}